﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema.Concert
{
    public enum Category
    {
        GoldTickets,
        SilverTickets,
        BronzeTickets
    }
    public class Bilet
    {
        public Category Category { get; set; }
        public float Price { get; }
        public Bilet() { }
        public Bilet(Category category) {
            if (category==Category.GoldTickets)
            {
                Category = category;
                Price = 600;
            }
            else if (category == Category.SilverTickets) {
                Category = category;
                Price = 400;
            }
            else if (category==Category.BronzeTickets)
            {
                Category = category;
                Price = 200;
            }
        
        }

        public override string ToString()
        {
            return $"The Category of the ticket is {Category} and the price is: {Price}\n";
        }
    }
}
