﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Tema.Concert
{
   
    public class CasaDeBilete
    {
        public delegate void SuplimentaryTickets(Bilet ticket,int numberTickets);
        public event SuplimentaryTickets _suplimentaryTickets;

        

        List<Bilet> Tickets = new List<Bilet>();
        public int NumberOfTicketsToSell;
        public int NumberOfTicketsToBuy;
        List<Client> clients= new List<Client>();

        public void AddTickets(Bilet ticket, int numberOfTicketsToSell)
        {
            for (int i = 0; i < numberOfTicketsToSell; i++)
            {
                Tickets.Add(ticket);
            }
            var originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("The stock has been supplied!");
            _suplimentaryTickets?.Invoke(ticket, numberOfTicketsToSell);
            Console.ForegroundColor = originalColor;
        }

        

        public void Transaction(Client client,Bilet bilet,int NumberOfTicketsToBuy) 
        {
            if ((bilet.Category == Category.GoldTickets && NumberOfTicketsToBuy < Tickets.Count(ticket => ticket.Category == Category.GoldTickets)) ||
                    (bilet.Category == Category.SilverTickets && NumberOfTicketsToBuy < Tickets.Count(ticket => ticket.Category == Category.SilverTickets)) ||
                    (bilet.Category == Category.BronzeTickets && NumberOfTicketsToBuy < Tickets.Count(ticket => ticket.Category == Category.BronzeTickets)))
            {
                var ceva = Tickets.Count(ticket => ticket.Category == Category.BronzeTickets);
                Console.WriteLine(ceva);
                switch (bilet.Category)
                {
                    case Category.GoldTickets:
                        Console.WriteLine($"You have to pay {NumberOfTicketsToBuy * bilet.Price}");
                        for (int i = 0;i < NumberOfTicketsToBuy; i++)
                        {
                            Tickets.Remove(bilet);
                        }
                        break;
                    case Category.SilverTickets:
                        Console.WriteLine($"You have to pay {NumberOfTicketsToBuy * bilet.Price}");
                        for (int i = 0; i < NumberOfTicketsToBuy; i++)
                        {
                            Tickets.Remove(bilet);
                        }
                        break;
                    case Category.BronzeTickets:
                        Console.WriteLine($"You have to pay {NumberOfTicketsToBuy * bilet.Price}");
                        for (int i = 0; i < NumberOfTicketsToBuy; i++)
                        {
                            Tickets.Remove(bilet);
                        }
                        break;
                    default: throw new ArgumentException("The category doesn't exist");
                }
            }

            Console.WriteLine("Do you want to proceed the transaction?");
            var word=Console.ReadLine();
            if (word =="Yes"||word=="yes") { 
                if ((bilet.Category==Category.GoldTickets&& NumberOfTicketsToBuy<Tickets.Count(ticket=>ticket.Category==Category.GoldTickets))||
                    (bilet.Category == Category.SilverTickets && NumberOfTicketsToBuy < Tickets.Count(ticket => ticket.Category == Category.SilverTickets))||
                    (bilet.Category == Category.BronzeTickets && NumberOfTicketsToBuy < Tickets.Count(ticket => ticket.Category == Category.BronzeTickets)))
                {
                    Console.WriteLine("Your transaction has been completed with succesed! See you at the concert!");
                }
                else
                {
                    Console.WriteLine("Your transaction has failed!\nSorry:( Our stock is empty! You are in the waiting list now!");
                    clients.Add(client);
                    foreach (var cl in clients)
                    {
                        _suplimentaryTickets = cl.Notification;
                    }
                }
            }
            else
            {
                Console.WriteLine("Bye Bye!");
            }
        }

        
        /*public void Supply()
        { 
            foreach (var client in clients) { }
            if (clients.Count == 5 && Tickets.Any())
                if (Tickets.Any(ticket => ticket.Category == Category.GoldTickets))
                {
                    var goldTickets = Tickets.Where(ticket => ticket.Category == Category.GoldTickets).Take(10).ToList();
                    foreach (var ticket in goldTickets)
                    {
                        AddTickets(ticket, 1);
                    }
                }
                else if (Tickets.Any(ticket => ticket.Category == Category.SilverTickets))
                { 
                    var silverTickets = Tickets.Where(ticket => ticket.Category == Category.SilverTickets).Take(10).ToList();
                    foreach (var ticket in silverTickets)
                    {
                        AddTickets(ticket, 1);
                    }
                }
                else if (Tickets.Any(ticket => ticket.Category == Category.BronzeTickets))
                {
                    var bronzeTickets = Tickets.Where(ticket => ticket.Category == Category.BronzeTickets).Take(10).ToList();
                    foreach (var ticket in bronzeTickets)
                    {
                        AddTickets(ticket, 1);
                    }
                }

        }*/
        public void ShowTheList()
        {
            foreach (Bilet ticket in Tickets)
            {
                Console.WriteLine(ticket);
            }
        }
        public void ShowTheClientsList()
        {
            foreach (Client client in clients)
            {
                Console.WriteLine(client.Name);
            }
        }
    }
}
