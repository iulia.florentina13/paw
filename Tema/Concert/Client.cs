﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema.Concert
{
    public class Client
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Client() 
        {
            this.Id = Guid.NewGuid();
            this.Name = string.Empty;
            this.Age = 0;
        }
        public Client( string Name, int Age) 
        {
            this.Id = Guid.NewGuid();
            this.Name = Name;
            this.Age = Age;
           
        }

        public string HasDiscount()
        {
            if (Age > 1 && Age < 28)
                return "The participant is a child, pupil or student and has 50% discount.";
            else if (Age > 65)
                return "The participant is pensionar with 25% discount.";
            else 
                return "The participant is not calificate for the discount. ";
        }
        public override string ToString()
        {
            return $" Id:{Id}\n Name:{Name}\n Age:{Age}\n Has Discount:{HasDiscount()} ";
        }

        public void Notification(Bilet ticket,int numberTickets)
        {
            var originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($"{numberTickets} tickets from {ticket} category has been added! Go check and buy some!");
            Console.ForegroundColor = originalColor;
        }
        public static Client client()
        {
            Console.WriteLine($"Welcome to the Festival! We need some information about you to buy some tickets!\n");
            Console.WriteLine("Please enter your name: ");
            string name= Console.ReadLine();
            Console.WriteLine("Please enter your age: ");
            int age = int.Parse(Console.ReadLine());

            return new Client(name, age);
        }

    }
}
